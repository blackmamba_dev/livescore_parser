# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from bs4 import BeautifulSoup
import json
import mysql.connector
from datetime import date
import time


_db_host = '127.0.0.1'
_db_user = 'root'
_db_password = 'root'
_db_name = 'livescore'
_db_table_name = 'matches'

connection = mysql.connector.connect(
    user=_db_user,
    password=_db_password,
    host=_db_host,
    database=_db_name,
    )

cursor = connection.cursor()


def prepare_insert_query(p_count):
    insert_query = """INSERT INTO __TABLE__ (`key`, `tournament`,
                   `player1`, `player2`, `date`, `type`, `set`,
                   `firstserve` __COLS__ )
                   VALUES (%(key)s, %(tournament)s, %(player1)s,
                   %(player2)s, %(date)s, %(type)s,
                   %(set)s, %(firstserve)s __VALUES__ )
                   """
    if p_count:
        cols = []
        values = []
        for i in range(1, p_count+1):
            cols.append('`p' + str(i) + '`')
            values.append('%(p'+str(i)+')s')

        insert_query = insert_query.replace('__COLS__', ' , ' + ' , '.join(cols))
        insert_query = insert_query.replace('__VALUES__', ' , ' + ' , '.join(values))
        insert_query = insert_query.replace('__TABLE__', _db_table_name)
    else:
        insert_query = insert_query.replace('__COLS__', '')
        insert_query = insert_query.replace('__VALUES__', '')
        insert_query = insert_query.replace('__TABLE__', _db_table_name)

    return insert_query


def select_by_key(key):
    cursor.execute("SELECT * FROM " + _db_table_name + " WHERE `key` = %s", (key,))
    return cursor.fetchall()


def parse_tennis_tables(soup):
    """
    :type soup: BeautifulSoup
    """
    matches_array = {}
    tables = soup.select('table.tennis')
    for table in tables:
        # print(table.select('td.head_ab span.tournament_part')[0].text)
        current_date = soup.select('li#ifmenu-calendar')[0].text
        current_date = current_date.split(' ')[0]
        current_date += '/' + str(date.today().year)
        current_date = '-'.join(current_date.split('/')[::-1])
        tournament = table.select('td.head_ab span.name span.tournament_part')[0].text
        country_part = table.select('td.head_ab span.name span.country_part')[0].text
        tournament_type = None
        if country_part.find("ATP") != -1:
            tournament_type = 'Man Singles'
        elif country_part.find("ATP") != -1:
            tournament_type = 'Women Singles'

        trs = table.select('tr')
        # live = 1
        for tr in trs:
            tr_id = tr.get('id')
            if not tr_id:
                continue
            match_id = str(tr_id).split('_')
            if len(match_id) != 3:
                continue

            team_home = tr.select('td.team-home')
            team_away = tr.select('td.team-away')
            parts = tr.select('td.part-bottom')
            if not parts:
                parts = tr.select('td.part-top')
            set = 0
            sets_count = 0
            tiebreaks_count = 0
            need_to_save = 0
            # checked = 0
            for part in parts:
                text = part.find(text=True, recursive=False)
                if text.isdecimal():
                    need_to_save = 1
                    # checked = 1
                sets_count += 1 if text.isdecimal() else 0
                if part.select('sup'):
                    set = sets_count
                    text = part.select('sup')[0].text
                    tiebreaks_count += int(text) if text.isdecimal() else 0

            match_id = match_id[2]
            if match_id not in matches_array:
                matches_array[match_id] = {'sets': 0, 'tiebreaks': 0}
            # matches_array.setdefault(match_id, match_id)
            matches_array[match_id]['need_to_save'] = need_to_save
            matches_array[match_id]['set'] = set
            matches_array[match_id]['date'] = current_date
            # matches_array[match_id]['sets'] = sets_count
            matches_array[match_id]['tiebreaks'] += tiebreaks_count
            matches_array[match_id]['key'] = match_id
            matches_array[match_id]['tournament'] = tournament
            matches_array[match_id]['type'] = tournament_type
            if team_home:
                matches_array[match_id]['player1'] = team_home[0].text
                # print(tournament + ' ' + team_home[0].text)
            if team_away:
                matches_array[match_id]['player2'] = team_away[0].text
                # print(tournament + ' ' + team_away[0].text)
            live = tr.select('td span span.clive')
            if live:
                matches_array[match_id]['live'] = 1

    return matches_array


def parse_tiebreaks(soup):
    """
    :type soup: BeautifulSoup
    """
    tiebreaks_array = []
    current_date = ''.join(soup.select('td#utime')[0].text.split(' ')[0])
    current_date = '-'.join(current_date.split('.')[::-1])
    # print(date)
    parts = soup.select('table.parts-first')
    for part in parts:
        html = str(part)
        tiebreaks = html.split('Tiebreak')
        # del tiebreaks[0]
        if len(tiebreaks) == 1:
            continue

        # sub_soup = BeautifulSoup('<table><tbody><tr><td>' + tiebreak, "lxml")
        sub_soup = BeautifulSoup(tiebreaks[1], "lxml")
        trs = sub_soup.select('tr')
        tb = []
        for tr in trs:
            score = tr.select('td.match-history-score span')
            if not score:
                continue
            # print(score[0].get('class'))
            p = 'A' if score[0].get('class')[0] == 'score-highlight' else 'B'
            score = score[0].text + '-' + score[1].text
            # serve =
            # print(unicode(tr.select('td.server')[0]))
            serve = 'A' if str(tr.select('td.server')[0]).find('tennis-serve') != -1 else 'B'
            # tiebreaks_array.append({'serve': serve, 'score': score, 'p': p})
            # print(json.dumps({'serve': serve, 'score': score, 'p': p}))
            tb.append({'serve': serve, 'score': score, 'p': p, 'date': current_date})

        tiebreaks_array.append(tb)

    return tiebreaks_array

driver = webdriver.Firefox()
driver.get("http://www.livescore.in/si/tenis/")
delay = 15
try:
    for i in range(1, 9):
        element = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'table.tennis')))
        time.sleep(1)
        # WebDriverWait(driver, 1)
        print("Page is ready!")
        html = driver.page_source
        # print(html)
        matches = parse_tennis_tables(BeautifulSoup(html, "lxml"))
        # print(json.dumps(matches))
        main_window = driver.current_window_handle
        for key, match in matches.items():
            records = select_by_key(key)
            if len(records):
                print('Record with key "' + key + '" already exists. Skip.')
                continue
            need_to_save = match['need_to_save']
            live = 1 if 'live' in match else 0
            p = 0
            row = match.copy()
            row.pop('need_to_save', None)
            row.pop('live', None)
            row.pop('sets', None)
            # row.pop('tiebreaks', None)
            if match['tiebreaks']:
                driver.execute_script("window.open('http://www.livescore.in/si/tekma/" + match['key'] + "/#tocka-za-tocko;1');")
                driver.switch_to.window(driver.window_handles[1])
                element = WebDriverWait(driver, delay).until(
                    EC.presence_of_element_located((By.CSS_SELECTOR, 'table.parts-first')))
                WebDriverWait(driver, 10)
                html = driver.page_source
                tiebreaks = parse_tiebreaks(BeautifulSoup(html, "lxml"))
                # print(tiebreaks)
                driver.close()
                driver.switch_to.window(main_window)
                for tiebreak in tiebreaks:
                    p = 1
                    row['firstserve'] = tiebreak[0]['serve']
                    row['date'] = tiebreak[0]['date']
                    for tb in tiebreak:
                        row['p' + str(p)] = tb['p']
                        p += 1
                    p -= 1
            else:
                row['set'] = None
                row['firstserve'] = None

            if live:
                need_to_save = 0

            if need_to_save == 1:
                print('Saving: ' + '|'.join(str(row[x]) for x in sorted(row)))
                insert_query = prepare_insert_query(p)
                cursor.execute(insert_query, row)
                connection.commit()
            else:
                add_str = ''
                if live:
                    add_str = ' (match not ended yet) '
                print('Not saving: ' + add_str + '|'.join(str(row[x]) for x in sorted(row)))

        driver.execute_script('set_calendar_date("-' + str(i) + '");')
        time.sleep(1)
        # WebDriverWait(driver, 1)
        WebDriverWait(driver, delay).until(EC.invisibility_of_element_located((By.ID, 'preload')))
        time.sleep(1)
        # WebDriverWait(driver, 1)
    print('Parsing finished.')

except TimeoutException:
    print("Loading took too much time!")
finally:
    driver.close()

